# Cube Black - Herelink Integration


### Herelink Overview

![](IMAGES/Herelink1.jpg)


Herelink is an integrated remote controller, ground station and wireless digital transmission system designed to be used with the Cube Autopilot, Ardupilot or PX4.
Herelink allows RC control, HD video and telemetry data to be transmitted upto 20km between the ground station and air unit, the Herelink remote controller features custom Solex TX and QGC applications and both air unit and ground station feature an integrated 8 core SOC for custom application development.
### Features

*Android based smart device with 5.46 inch 1080P capacitive touch screen.

*Integrated digital transmission system simultaneously transmitting video, telemetry and control. 

*Custom Solex TX and QGC ground station software with MAVlink support for Ardupilot and PX4.

*Dual hall effect self centering gimbals with removable sticks.

*1080P HD video with dual digital video inputs.

*Dual S.bus signal output for simultaneous control of autopilot and pan/tilt cameras.

*6 customisable Mavlink buttons and Mavlink/Sbus hardware wheel.

*RC gimbal control on dedicated service independent from Solex TX / QGC.

*Tether and share video and telemetry via wifi ground station such as Mission Planner.

*Onboard Arm Cortex A53 & Cortex A53 SOC in both air and ground units for custom applications. 

## Technical specifications

### Ground & Air Unit:

Feature             |  Specification  
--------------------|----------
SOC                 |   ARM Cortex A53 4 core 2.2GHz + Cortex A53 4 core 1.4GHz   
Image Processing:         |   Mali-T860 GPU   
SDR    |   A7 + DSP  
Memory |   Air Unit: LPDDR3: 1GB, Ground Station: LPDDR3: 2GB, EMMC: Air / Ground 4GB
Transmission Distance:   |   FCC 20km CE / SRRC 12km Image transmission delay: ≤ 110ms Image transmission: 720p@30fps 1080p@30 / 60fps transmission,signal bandwidth 20MHz / 10MHz

### Ground Station:

![Herelink - Ground Station](IMAGES/Herelink2.jpg)

Feature          |  Specification  
--------------------|----------
Screen              |   5.46 inch 1080P, 16 million colors, capacitive touch screen   
Remote control:         |   Hall affect gimbals with removable sticks x 2, scroll wheel × 1, bottom button × 6 with backlight, top button × 1 (right)  
Wireless    |   Bluetooth / WIFI / GPS / 2.4G map transmission ground
Power : |   Built-in 4950 mAh Lipo Battery
Power Consumption   |   The average power consumption does not exceed 4W (only picture transmission work, medium screen brightness, WiFi off, GPS off)


## Pairing, RC Calibration & Setup

Herelink has a dedicated system settings app for the setup and configuration of the Herelink RC control and radio system.


The Herelink Settings app allows you to

Pair The Air Unit 

See Radio & System Status

Calibrate the RC

Set RC Mode & Throttle Settings 

Calibrate HW Wheel and Set Channel

Set Radio Regional Settings

Configure Sbus Buttons

The following tutorial will guide you through the steps to setup and calibrate the Herelink system. For button setup there are dedicated pages in this wiki for Mavlink and Sbus button options, these should only be completed after performing the steps on this page.


### Pairing with Air Unit

Tap Pair button and then hold the Pair/Reset button on Air Unit until LED2 blinks.

![](IMAGES/settings101.png)
![](IMAGES/airpair1.png)
![](IMAGES/airpair2.png)
### Joystick and HW Wheel Calibration

Swipe left to access the joystick screen.
![](IMAGES/Stick_screen.jpg)
Here you will find the sticks and hardware wheel options and calibration settings.

From this screen you can

See RC Calibration Status 

Calibrate Hardware Wheel

Calibrate Sticks

Set RC Stick Mode

Set Hardware Wheel Sbus Channel & Bus

Reverse RC Channel

Set Throttle Centre Behavior


### Step 1: Calibrate the hardware wheel

Select 'HW WHEEL CAL'.

![](IMAGES/wheel_cal.jpg)
Click 'Start rollwheel'  and follow the calibration steps

Note once complete you can check its correct functioning by looking at the values change, click Pass to return to joystick screen

![](IMAGES/hw-wheel.png)
Once calibration is complete you can set the Sbus channel output and Bus output for the wheel on the highlighted settings
Bus 1 is the same output as the sticks and the wheel can be set to channels 5 - 16, on bus 2 the wheel can be set to channels 1 -16. 
![](IMAGES/Wheel_ch.jpg)
Once complete click 'SAVE' to store settings. 

### Step 2: Calibrate the joysticks

Click 'HW JS CAL'
![](IMAGES/Stick_cal.jpg)
This screen is split into 2 sections, joystick calibration on the left, joystick testing on the bottom right, instructions for the user to follow will be shown in the green box.  Click 'Start Calibration' to begin the process and follow the instructions in the green box  and the sick movement arrows located around the sticks on the remote as highlighted in red below.

Note: The joystick testing area in the bottom right will not move or show any input while you are calibrating the sticks. This is normal, this area will only diplay input after the joysticks have been calibrated.

![](IMAGES/joystick-cal1.jpg)
![](IMAGES/joystick-testnote.jpg)
Once the joystick calibration step is complete the input test area in the red box will activate, follow the instructions above it to test the sticks input are functioning correctly by aligning the black and red circles in each step shown. This will test the joysticks are moving correctly in all axises.

At the end of this process if you are happy with the input behaviour you can click ‘Pass’ and the calibration will be stored, if you're experiencing any issues click ‘Fail’ and start the calibration process again using a little more pressure in the corners.

![](IMAGES/joystick-test.jpg)

### Step 3 RC Mode Selection , Throttle Behavior & Channel Reversing

Herelink supports rc modes 1 - 4 as well as the option to set the throttle centre as zero PWM output and reverse each channeel.

Set throttle center and RC mode via the below settings. 
![](IMAGES/Rc_mode.jpg)
The stick sbus output can be reversed by clicking on ‘REV’ next to the channel you want to change.
![](IMAGES/Channel_rev.jpg)
After making any changes click ‘SAVE” to store settings. 

### Step 4 Calibrate SBUS Output
To calibrate the joystick SBUS outpiuts click 'SBUS OUT CAL'. 
![](IMAGES/Sbus_cal.jpg)
Follow the RC stock movement steps as shown in the highlight section moving the stick through each position.

![](IMAGES/Sbus_cal2.png)
![](IMAGES/Sbus_cal1.jpg)
Once complete click ‘SAVE” to store settings. 

## Selecting FCC/CE settings
Select the region you are residing in or matches closest to your locations policy from Country Under Herelink Settings main screen.
![](IMAGES/fccsettings.png)
